# create migrations

To create a new migration firstly run:
```
$env:DATABASEURL = 'mysql+mysqlconnector://user:password@host:3306/database'
$env:PYTHONPATH = Get-Location
alembic revision --autogenerate -m "{migration name}"
```

# running migrations

To run migrations do:
```
$env:DATABASEURL = 'mysql+mysqlconnector://user:password@host:3306/database'
$env:PYTHONPATH = Get-Location
alembic upgrade head
```