from sqlalchemy import Table, Column, Integer, ForeignKey
from common.DAO import DAO
#Pivot Table
Base = DAO.get_base_mapper()

RecipeIngredients = Table('recipe_ingredients', Base.metadata,
    Column('recipe_id', Integer, ForeignKey('recipe.id')),
    Column('ingredient_id', Integer, ForeignKey('ingredient.id'))
    )



