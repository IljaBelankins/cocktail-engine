from sqlalchemy import Column, ForeignKey, String, Integer
from sqlalchemy.orm import relationship
from common.DAO import DAO
from models.recipe_ingredients import RecipeIngredients
from typing import Optional, List

Base = DAO.get_base_mapper()

class Recipe(Base):
    """
    Contains basic information about the cocktail recipe.
    """

    __tablename__ = "recipe"

    id: int = Column (
        Integer,
        primary_key=True,
        nullable=False
    )

    name: str = Column(
        String(200),
        nullable=False,
        index=True
    )

    instructions: str = Column(
        String(1000),
        nullable=True
    )

    image: str = Column(
        String(2000),
        nullable=True
    )

    measurements: str = Column(
        String(3000),
        nullable=True
    )

    ingredients = relationship("Ingredient", secondary=RecipeIngredients)

    def to_dict (self, ingredients: Optional[List[int]]):
        missing_ingredients = 0
        if ingredients is not None:
            recipe_ingredients = []
            for item in self.ingredients:
                recipe_ingredients.append(item.id)
            ingredients = set(ingredients)
            recipe_ingredients = set(recipe_ingredients)
            missing_ingredients = len(recipe_ingredients-ingredients)
        return {
            "id": self.id,
            "name": self.name,
            "instructions": self.instructions,
            "image": self.image,
            "measurements": self.measurements,
            "missing_ingredients": missing_ingredients
        }

    def __repr__(self):
        return str(self.to_dict())