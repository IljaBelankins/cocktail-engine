from sqlalchemy import Column, ForeignKey, String, Integer
from sqlalchemy.orm import relationship
from common.DAO import DAO
from models.recipe_ingredients import RecipeIngredients

Base = DAO.get_base_mapper()

class Ingredient(Base):
    """
    Contains information about an ingredient.
    """

    __tablename__ = "ingredient"

    id: int = Column (
        Integer,
        primary_key=True,
        nullable=False
    )

    name: str = Column(
        String(200),
        nullable=False,
        index=True,
    )

    recipes = relationship("Recipe", secondary=RecipeIngredients)

    def to_dict (self):
        return {
            "id": self.id,
            "name": self.name
        }
