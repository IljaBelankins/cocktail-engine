from flask import Flask, render_template, request, jsonify, send_from_directory
from models.ingredients import Ingredient
from models.recipe_ingredients import RecipeIngredients
from models.recipes import Recipe
from models import models
from common.DAO import DAO
import os

app = Flask (__name__)

@app.route("/search", methods=['POST'])
def search():

    with DAO.session_scope(os.getenv("DATABASEURL")) as session:
        submitted_ingredients = request.form.getlist('ids[]')
        query_option = request.form.get('query_option')
        if query_option == 'including':
            found_recipes = session.query(RecipeIngredients.c.recipe_id).filter(RecipeIngredients.c.ingredient_id.in_(submitted_ingredients)).all()
        else:
            unused_ids = session.query(Ingredient.id).filter(Ingredient.id.notin_(submitted_ingredients)).all()
            converted_unused_ids = []
            for item in unused_ids:
                converted_unused_ids.append(item[0])
            invalid_recipe_ids = session.query(RecipeIngredients.c.recipe_id).filter(RecipeIngredients.c.ingredient_id.in_(converted_unused_ids)).all()
            converted_invalid_recipe_ids = []
            for item in invalid_recipe_ids:
                converted_invalid_recipe_ids.append(item[0])
            found_recipes = session.query(RecipeIngredients.c.recipe_id).filter(RecipeIngredients.c.ingredient_id.in_(submitted_ingredients)).filter(RecipeIngredients.c.recipe_id.notin_(converted_invalid_recipe_ids)).all()
        recipe_ids = []
        for recipe in found_recipes:
            recipe_ids.append(recipe[0])
        #print (recipe_ids)
        final_recipes = session.query(Recipe).filter(Recipe.id.in_(recipe_ids)).all()
        dict_recipes = []
        ingredient_ids = []
        for item in submitted_ingredients:
            ingredient_ids.append(int(item))
        for item in final_recipes:
            dict_recipes.append(item.to_dict(ingredient_ids))
        return jsonify(results=dict_recipes)


@app.route("/")
def engine():

    with DAO.session_scope(os.getenv("DATABASEURL")) as session:
        
        fresh_ingredients = session.query(Ingredient).order_by(Ingredient.name).all()
        #The joke here is that fresh ingredients cannot be stored. Therefore the
        #the second list is just called "ingredients".
        ingredients = []
        for ingredient in fresh_ingredients:
            ingredients.append(ingredient.to_dict())

        

    return render_template("layout.html", ingredients = ingredients)

@app.route('/media/<path:filename>')
def download_file(filename):
    return send_from_directory("media", filename)

if __name__ == '__main__':
    app.run()