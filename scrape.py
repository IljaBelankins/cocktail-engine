import string
from models.recipes import Recipe
from common.DAO import DAO
from models import models
from models.ingredients import Ingredient
import os
import requests

with DAO.session_scope(os.getenv("DATABASEURL")) as session:
    abc = string.ascii_lowercase
    for letter in abc:
        url = f'https://www.thecocktaildb.com/api/json/v1/1/search.php?f={letter}'
        headers = {}
        r = requests.get(url, headers=headers)
        response_dict = r.json()
        cocktail_list = response_dict['drinks']
        if not cocktail_list:
            continue
        for cocktail in cocktail_list:
            
            measurements = []
            ingredients_list = []
            found_ingredients = []

            for i in range(1, 16):
                if cocktail[f"strIngredient{i}"]:
                    print (cocktail[f"strIngredient{i}"])
                    ingredient_check = session.query(Ingredient).filter(Ingredient.name==cocktail[f"strIngredient{i}"].lower()).one_or_none()
                    if ingredient_check is None:
                        ingredient_check = Ingredient(name = cocktail[f"strIngredient{i}"].lower())
                    if cocktail[f"strIngredient{i}"].lower() not in found_ingredients:
                        ingredients_list.append(ingredient_check)
                        found_ingredients.append(cocktail[f"strIngredient{i}"].lower())
                    if cocktail[f"strMeasure{i}"] is not None:
                        measurements.append(
                            cocktail[f"strIngredient{i}"]+" - "+cocktail[f"strMeasure{i}"]
                        )
                    else:
                        measurements.append(
                            cocktail[f"strIngredient{i}"]
                        )
            measurements = ", ".join(measurements)

            new_recipe = Recipe(
                name=cocktail["strDrink"],
                instructions=cocktail["strInstructions"],
                image=cocktail["strDrinkThumb"],
                measurements=measurements
            )
            for ingredient in ingredients_list:
                new_recipe.ingredients.append(ingredient)
                session.add(ingredient)
            session.add(new_recipe)
            session.commit()

