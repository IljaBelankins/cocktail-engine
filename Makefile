export DATABASEURL = mysql+mysqlconnector://root:password@localhost:3306/cocktail

start_database:
	docker-compose -f docker-compose-db.yaml up

run_migration:
	alembic upgrade head

scrape_data:
	python scrape.py

start_api:
	python api.py

